#include <iostream>

using namespace std;

template <typename A> 
class Vector3d
{
	public:

		A x,y,z;

		Vector3d(A x, A y, A z)
		{
			this->x = x;
			this->y = y;
			this->z = z;
		}

		void display()
		{
			cout << x << "i+";
			cout << y << "j+";
			cout << z << "k";
		}

		static A dot(Vector3d v1, Vector3d v2)
		{
			A ans;
			ans = v1.x*v2.x+v1.y*v2.y+v1.z*v2.z;
			return ans;
		}
};

int main()
{
	Vector3d <float> u(2.4,3.6,1.3), v(1.1,3.0,4.5);
	Vector3d <char> w(82,106,67), d(1,0,5);

	u.display();
	cout << endl ;
	v.display();
	cout << endl ;
	w.display();
	cout << endl ;
	d.display();
	cout << endl ;

	cout << "their scalar product is " << Vector3d<float>::dot(u,v);
	cout << endl ;
}