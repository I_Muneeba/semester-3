#include <iostream>
#include <set>

using namespace std;


int main()
{
	set<float> u;
	
	u.insert(34.777777);
	u.insert(655.77);
	u.insert(320.007);
	u.insert(40.007);
	u.insert(8860.007);
	u.insert(50.007);
	u.insert(30.007);
	u.insert(10.007);
	
	cout << u.size() << endl; 
	
	set<float>::iterator it;
	for (it=u.begin(); it!=u.end(); ++it)
    cout << endl << *it;
	
/*
	for(int i=0; i<u.size(); i++)
	{
		cout << u[i] << endl;
	}
*/	
	cout << endl ;
}