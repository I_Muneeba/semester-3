#include<iostream>

using namespace std;

template <typename Y>
Y GetMax(Y a, Y b)
{
	return (a>b?a:b);
}

template <class Y>
void interchange (Y &a, Y &b)
{
     Y t;
     t=a;
     a=b;
     b=t;
}

int main()
{
    float x=float(10);
    float y=float(1.25);

    if(GetMax(x,y)==x)
    {
    	interchange(x,y);
    }

    cout<<"The Value of x is   : "<<x << endl<<"The Value of y is   : "<<y<<endl;
    system("pause");
    return 0;
}
