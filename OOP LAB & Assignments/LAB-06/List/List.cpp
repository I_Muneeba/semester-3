#include "List.h"
#include <iostream>
#include <iomanip>

using namespace std;

void initializeList(List &l)
{
	l.array = new double[l.MAX_SPARE_ENTRIES];
	l.arraySize = l.MAX_SPARE_ENTRIES;
	l.listSize = 0;
}

void disposeList(List &l)
{
	delete[] l.array;
	l.array = NULL;
	l.arraySize = 0;
	l.listSize = 0;
}

void makeListEmpty(List &l)
{
	l.listSize = 0;
}


void showList(const List &lst)
{
	for(int j=0; j<lst.listSize; j++)
		cout << fixed << setprecision(2) << lst.array[j] << " ";
	cout << endl;
}

void append(List &l, double d)
{
	if(isFull(l)) throw -1;
	l.array[l.listSize++] = d;
}


bool isEmpty(const List &l)
{
	return l.listSize==0;
}

bool isFull(const List &l)
{
	return l.listSize==l.arraySize;
}
/*
bool equal(const List &l1, const List &l2)
{
	// return true, if l1 and l2 are exactly same upto arraySize
}

bool search(const List &l, double d)
{
	// return true if d is somewhere in List l (upto arraySize)
}


int countOf(const List &l, double d)
{
	// return count of values matches d in list l (upto arraySize)
}


int indexOfFirstMatch(const List &, double)
{
	// 
}

int indexOfLastMatch(const List &, double)
{
}

int indexOfMatchAfter(const List &, double, int)
{
}


void removeAt(List &, int)
{
}

void removeFirstMatch(List &, double)
{
}

void removeAll(List &, double)
{
}


void insertAt(List &, double, int)
{
}

void insertAtbegining(List &, double)
{
}

void insertBefore(List &, double, double)
{
}

void insertAfter(List &, double, double)
{
}


void updateAt(List &, double, int)
{
}

void updateFirstMatch(List &, double, double)
{
}

void updateAll(List &, double, double)
{
}


List join(const List &, const List &)
{
}

List subList(const List &, int, int)
{
}


// following can only be made for numbers
double max(const List &)
{
}

double min(const List &)
{
}

double sum(const List &)
{
}

double product(const List &)
{
}

double mean(const List &)
{
}


List wholeNumbers(const List &)
{
}
*/