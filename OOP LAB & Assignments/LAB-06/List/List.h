#ifndef NUM_LIST_VER_1
#define NUM_LIST_VER_1

struct List
{
	const int MAX_SPARE_ENTRIES  = 100;
	int arraySize;
	int listSize;
	double *array;
};

void initializeList(List &);
void disposeList(List &);
void makeListEmpty(List &);

void showList(const List &);
void append(List &, double);

bool isEmpty(const List &);
bool isFull(const List &);
bool equal(const List &, const List &);
bool search(const List &, double);

int countOf(const List &, double);

int indexOfFirstMatch(const List &, double);
int indexOfLastMatch(const List &, double);
int indexOfMatchAfter(const List &, double, int);

void removeAt(List &, int);
void removeFirstMatch(List &, double);
void removeAll(List &, double);

void insertAt(List &, double, int);
void insertAtbegining(List &, double);
void insertBefore(List &, double, double);
void insertAfter(List &, double, double);

void updateAt(List &, double, int);
void updateFirstMatch(List &, double, double);
void updateAll(List &, double, double);

List join(const List &, const List &);
List subList(const List &, int, int);

// following can only be made for numbers
double max(const List &);
double min(const List &);
double sum(const List &);
double product(const List &);
double mean(const List &);

List wholeNumbers(const List &);

#endif
