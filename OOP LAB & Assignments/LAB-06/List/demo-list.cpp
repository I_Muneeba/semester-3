#include "List.h"
#include <iostream>

using namespace std;

int main()
{
	List nums;
	initializeList(nums);
	
	append(nums, 3.45);
	append(nums, 2.72);
	append(nums, 3.91);
	append(nums, 2.00);
	append(nums, 2.75);
	append(nums, 2.25);
	append(nums, 3.31);
	append(nums, 3.83);
	append(nums, 2.43);
	append(nums, 1.83);
	append(nums, 2.94);
	append(nums, 3.65);
	append(nums, 2.98);

	showList(nums);

	disposeList(nums);
	return 0;
}
