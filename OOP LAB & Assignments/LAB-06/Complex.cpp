#include <iostream>
#include <math.h>
using namespace std;

class Complex
{
private:
	double realValue;
	double imagValue;
public:
	double getRealValue() const
	{
		return realValue;
	}

	double getImagValue() const
	{
		return imagValue;
	}

	void setRealValue(double r)
	{
		realValue =r;
	}
	void setImagValue(double i)
	{
		imagValue = i;
	}
	
	void display() const
	{
		cout << realValue << "," << imagValue;
	}
	
	Complex conjugate() const
	{
		Complex t;
		t.realValue = realValue;
		t.imagValue = -imagValue;
		return t;
	}
	
	void setComplex(double r, double i)
	{
		realValue = r;
		imagValue = i;
	}
	
	Complex operator+(const Complex &rhs) const
	{
		Complex res;
		
		res.realValue = realValue + rhs.realValue;
		res.imagValue = imagValue + rhs.imagValue;
		
		return res;
	}

	Complex operator-(const Complex &rhs) const
	{
		Complex res;
		
		res.realValue = realValue - rhs.realValue;
		res.imagValue = imagValue - rhs.imagValue;
		
		return res;
	}

	Complex& operator=(const Complex &rhs)
	{
		if(this != &rhs)
		{
			this->realValue = rhs.realValue;
			this->imagValue = rhs.imagValue;
		}
		return *this;
	}
	
};


void main()
{
	Complex w;
	w.setComplex(-4,2);
	Complex x;
	x.setComplex(2,8);
	Complex y;
	y.setComplex(3,-1);
	Complex z;
	
	//z = x.operator+(y).operator-(w);
	z = x + y - w;
	
	w.display();
	cout << endl;
	x.display();
	cout << endl;
	y.display();
	cout << endl;
	z.display();
	cout << endl;
}

