#include <iostream>
#include <utility>
#include <thread>
#include <chrono>

using namespace std;
 
void f1(int n, int m, int p)
{
    for (int i = 0; i < 5; ++i) {
        cout << "Thread 1 executing\n";
        n = ++n+m+p++;
        this_thread::sleep_for(chrono::milliseconds(2000));
    }
}
 
int main()
{
    int n = 0;
    thread th(f1, n + 1, 9, 4);
    for (int i = 0; i < 5; ++i) {
        cout << "Main executing\n";
        ++n;
        this_thread::sleep_for(chrono::milliseconds(1000));
    }
    th.join();
    cout << "Bye\n";
	 this_thread::sleep_for(chrono::milliseconds(1000));
	 return 0;
}