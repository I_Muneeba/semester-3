#include <iostream>
#include <sstream>
#include <string>
#include "SocketClient.h"

SocketClient::SocketClient(std::string ip, int port)
{
    this->ip=ip;
    this->port=port;
    this->connected=false;
    initParameters();
    initSocket(ip, port);
}

SocketClient::SocketClient(SOCKET so_cket)
{
    this->so_cket=so_cket;
    this->connected=true;
    initParameters();
}

SocketClient::SocketClient(){}

void SocketClient::initSocket(std::string ip, int port)
{
    WSAStartup(MAKEWORD(2,2), &WSAData);
    this->so_cket = socket(AF_INET, SOCK_STREAM, 0);
    this->addr.sin_addr.s_addr = inet_addr(ip.c_str());
    this->addr.sin_family = AF_INET;
    this->addr.sin_port = htons(port);
}

void SocketClient::initParameters()
{
    this->bytes_for_package_size=16;
    this->size_of_packages=2048;
    this->callback=NULL;
    this->callbackError=NULL;
    this->thread_started=false;
    this->errorWhileReceiving=false;
    this->errorWhileSending=false;
}

int SocketClient::connectServer()
{
    int r = connect(so_cket, (SOCKADDR *)&addr, sizeof(addr));
    if(r==SOCKET_ERROR)
    {
        if(callbackError!=NULL)
        {
            errorStruct error(*this, WSAGetLastError(), "Error while connecting.");
            callbackError(&error);
        }
    }
    else
        connected=true;

    return r;
}

void SocketClient::closeClient()
{
    if(thread_started)
        removeMessageCallback();
    removeErrorCallback();
    closesocket(so_cket);
    WSACleanup();
}

void SocketClient::sendString(std::string message)
{
    std::stringstream ss;
    ss << message;
    sendStream(ss);
}

void SocketClient::sendStream(std::istream &stream)
{
    char *buffer = new char[size_of_packages];
    int streamsize, p, result;
    std::stringstream ss;
    std::string strSize;

    stream.seekg(0, std::ios::end);
    streamsize = stream.tellg();
    stream.seekg(std::ios::beg);

    ss << streamsize;
    strSize = ss.str();

    while(strSize.size()<bytes_for_package_size)
    {
        strSize="0"+strSize;
    }

    result = send(so_cket, strSize.c_str(), bytes_for_package_size, 0);
    if(errorSending(result)){
		 delete[] buffer;
       return;
	}

    for (unsigned int i=0 ; i<streamsize/size_of_packages ; i++)
    {
        stream.read(buffer, size_of_packages);
        result = send(so_cket, buffer, size_of_packages, 0);
        if(errorSending(result)){
			  delete[] buffer;
           return;
		  }

        memset(buffer, 0, size_of_packages);
    }

    p=streamsize%size_of_packages;
    if(p!=0)
    {
        char *buff = new char[p];
        stream.read(buff, p);
        result = send(so_cket, buff, p, 0);
        if(errorSending(result)){
			  delete[] buffer;
			  delete[] buff;
           return;
		  }
    }
	  delete[] buffer;
}

bool SocketClient::errorSending(int result)
{
    if(result==SOCKET_ERROR)
    {
        if(callbackError!=NULL)
        {
            errorStruct error(*this, WSAGetLastError(), "Error while sending.");
            callbackError(&error);
        }
        connected=false;
        errorWhileSending=true;
        return true;
    }
    return false;
}

std::string SocketClient::receiveString()
{
    char *buffer = new char[bytes_for_package_size];
    int result = recv(so_cket, buffer, bytes_for_package_size, 0);

    if(errorReceiving(result)){
			delete[] buffer;
			return "";
	 }
    std::string messageSize(buffer, bytes_for_package_size);
    std::stringstream ss;
    int n;

    ss << messageSize;
    ss >> n;

    Sleep(1);

    std::string message;
    unsigned int nbLoop = n%size_of_packages==0?n/size_of_packages:n/size_of_packages+1;
    for (unsigned int i=0 ; i<nbLoop ; i++)
    {
        char* buff = new char[size_of_packages]();
        result = recv(so_cket, buff, size_of_packages, 0);

        if(errorReceiving(result))
            return "";

        message+=std::string(buff, result);
        delete[] buff;
    }
	 delete[] buffer;

    return message;
}

bool SocketClient::errorReceiving(int result)
{
    if(result==0 || result<0)
    {
        if(callbackError!=NULL)
        {
            errorStruct error(*this, WSAGetLastError(), "Receive failed.");
            callbackError(&error);
        }
        connected=false;
        errorWhileReceiving=true;
        return true;
    }
    return false;
}

void SocketClient::setSize_of_packages(unsigned int n)
{
    this->size_of_packages=n;
}

void SocketClient::setBytes_for_package_size(unsigned int n)
{
    this->bytes_for_package_size=n;
}

void SocketClient::setMessageCallback(void (*callback_function)(messageStruct *))
{
    callback = callback_function;
    thread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)messageThread, this, 0, 0);
    thread_started=true;
}

void SocketClient::removeMessageCallback()
{
    CloseHandle(thread);
    callback = NULL;
    thread_started=false;
}

void SocketClient::setErrorCallback(void (*callback_function)(errorStruct *))
{
    callbackError = callback_function;
}

void SocketClient::removeErrorCallback()
{
    callbackError = NULL;
}

void SocketClient::startThread()
{
    while(1)
    {
        if(connected)
        {
            std::string message = this->receiveString();
            if(errorWhileReceiving)
            {
                errorWhileReceiving=false;
            }
            else if(errorWhileSending)
            {
                errorWhileSending=false;
            }
            else
            {
                messageStruct m(*this, message);
                callback(&m);
            }
        }
    }
}

bool SocketClient::isConnected()
{
    return connected;
}
