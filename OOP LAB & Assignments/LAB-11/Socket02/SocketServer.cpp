#define _WINSOCK_DEPRECATED_NO_WARNINGS

#include "SocketServer.h"

SocketServer::SocketServer(int port)
{
    this->port=port;

    WSAStartup(MAKEWORD(2,2), &WSAData);
    server = socket(AF_INET, SOCK_STREAM, 0);

    addr.sin_addr.s_addr = INADDR_ANY;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);

    bind(server, (SOCKADDR *)&addr, sizeof(addr));
}

SOCKET SocketServer::acceptClient()
{
    listen(server, 3);
    SOCKET client;
    SOCKADDR_IN clientAddr;
    int clientAddrSize = sizeof(clientAddr);

    if((client = accept(server, (SOCKADDR *)&clientAddr, &clientAddrSize)) != INVALID_SOCKET)
    {
        return client;
    }
    return -1;
}

void SocketServer::closeServer()
{
    closesocket(server);
}
