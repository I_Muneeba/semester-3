// cl main_client.cpp SocketClient.cpp

#include <iostream>
#include <fstream>
#include "SocketClient.h"

using namespace std;

void onError(errorStruct *e)
{
    cout << e->message << endl;
}

int main()
{
    SocketClient client("127.0.0.1", 5555);
    client.setErrorCallback(onError);
    client.connectServer();

    string str;
    while(str != "z")
    {
        cout << ">";
        getline(cin, str);
        client.sendString(str);
    }
    
    //  send streams this way
    ifstream file("123.txt");
    client.sendStream(file);      // The server will receive the STRING CONTENT of the file.
    file.close();
    
    client.closeClient();
}
