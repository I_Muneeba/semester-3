#include <iostream>
#include <iomanip>

using namespace std;

class List
{
	const int MAX_SPARE_ENTRIES;
	int arraySize;
	int listSize;
	double *array;
	
	public:
	List() : MAX_SPARE_ENTRIES(100)
	{
		arraySize = MAX_SPARE_ENTRIES;
		listSize = 0;
		array = new double[arraySize];
	}
	
	List(int size): MAX_SPARE_ENTRIES(100)
	{
		cout << "we are here" << endl ;
		arraySize = MAX_SPARE_ENTRIES+size;
		listSize = 0;
		array = new double[arraySize];
	}
	
	//copy constructor
	List(const List &rhs): MAX_SPARE_ENTRIES(100)
	{
		arraySize = rhs.arraySize;
		listSize = rhs.listSize;
		array = new double[arraySize];
		for (int j=0; j<listSize; j++)
		{
			array[j] = rhs.array[j];
		}
	}
	
	~List()
	{
		delete[] array;
	}
	
	void append(double d)
	{
		array[listSize++] = d;
	}
	
	void showList() const
	{
		for(int j=0; j<listSize; j++)
			cout << fixed << setprecision(2) << array[j] << " ";
		cout << endl;
	}
};



int main()
{
	List nums;
	nums.append(23);
	nums.append(44);
	nums.append(74);
	nums.append(61);
	nums.append(58);
	
	nums.showList();
}
	