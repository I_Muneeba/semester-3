#include "iostream"
#include "fstream"
#include "sstream"
#include "string"

using namespace std;

//following are forward declarations, just ignore them
class Cashmemo;
class BillEntry;
class BillEntry2;
class ListEntries;
class Item;
// you may need to copy paste Cashmemo to Cashmemo2,
// ListEntries to ListEntries2 and modify them for task 2


// complete the following
class Cashmemo
{

};

class BillEntry
{

};

class BillEntry2
{

};

class ListEntries
{

};

class Item
{

};

int loadItems(Item *ar, int arSize)
{
	ifstream itemFile("Lab-08-Fall18-OOP-BCSF17-Items.txt");
	string itemLine;
	istringstream itemLineStringStream;
	int lineNo = 0;
	getline(itemFile, itemLine);
	while(itemLine.length() > 0)
	{
		lineNo++;
		itemLineStringStream.clear();
		itemLineStringStream.str("");
		itemLineStringStream.str(itemLine);

		int itemId;
		string itemDesc;
		float itemPrice;
		
		itemLineStringStream >> itemId;
		itemLineStringStream.ignore(1);
		getline(itemLineStringStream, itemDesc, ',');
		itemLineStringStream.ignore(1);
		itemLineStringStream >> itemPrice;
		itemLineStringStream.ignore(1);
		
		cout << itemId << " ~ " << itemDesc << " ~ " << itemPrice << endl;
		itemLine = "";
		getline(itemFile, itemLine);
	}
	itemFile.close();
	return lineNo;
}

int mainTask1()
{
	Cashmemo cm;
	cin >> cm;
	cout << cm;

	return 0;
}

int mainTask2()
{
	Item *items = new Item[100];
	int itemCount = loadItems(items, 100);
	
	Cashmemo cm(items, itemCount);
	cin >> cm;
	cout << cm;
	
	return 0;
}


int main()
{
	return mainTask1();
	//return mainTask2();
}