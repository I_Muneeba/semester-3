#include "Socket.h"

#include <iostream>

using namespace std;

int main() {

  try {
	 /*
    SocketClient s("www.pu.edu.pk", 80);

    s.SendLine("GET / HTTPS/1.0");
    s.SendLine("Host: www.pu.edu.pk");
    s.SendLine("");
	 */
	 
    SocketClient s("google.com", 80);

    s.SendLine("GET /search?q=PUCIT HTTP/1.1");
    s.SendLine("Host: www.google.com");
    s.SendLine("User-Agent: MyProg: 73.3");
    s.SendLine("Accept: text/html");
    s.SendLine("");

    while (1) {
      string l = s.ReceiveLine();
      if (l.empty()) break;
      cout << l;
      cout.flush();
    }

  } 
  catch (const char* s) {
    cerr << s << endl;
  } 
  catch (std::string s) {
    cerr << s << endl;
  } 
  catch (...) {
    cerr << "unhandled exception\n";
  }

  return 0;
}
