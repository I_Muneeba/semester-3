These files are for two separate projects.

1. To run on a Server computer
   Add all file to project except main_client.cpp
	Compile and execute it.
	It may open a dialog for access, allow it.
	It will go into a wait state for a client.

2. To run on a client computer (may be same, here it must).
	Open, another Visual Studio application
   Add all file to project except main_server.cpp
	Compile and execute it. 
	It will show > in command window.
	Type any string and press enter.
	Whatever you entered will also get displayed on
	server machine command window.
	Enter z to break the loop.

	To run on separate computers, in first line of
	clients main, you must change the IP address from
	127.0.0.1 to that of server computer. Ask LAB
	staff to help.
