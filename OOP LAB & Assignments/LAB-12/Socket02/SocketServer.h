#ifndef SOCKETSERVER_H
#define SOCKETSERVER_H

#include <winsock2.h>

#pragma comment(lib,"ws2_32.lib")

class SocketServer
{
    public:
        SocketServer(int port);
        SOCKET acceptClient();
        void closeServer();
    private:
        int port;
        WSADATA WSAData;
        SOCKET server;
        SOCKADDR_IN addr;
};

#endif // SOCKETSERVER_H
